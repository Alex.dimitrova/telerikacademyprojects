Meta:

Narrative:
As a user
I want to be able create topics
So that I can share my opinion with other users of the forum

Scenario: I want to be able to create topics
Given element schoolForumLogo is present
When click newTopicButton element
And type topicTitle in titleField field
And type topicSubject in textArea field
When click createTopicButton element
Then element getNewPostTitle is present