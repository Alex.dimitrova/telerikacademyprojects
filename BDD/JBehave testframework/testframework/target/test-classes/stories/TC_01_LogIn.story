Meta:

Narrative:
As a user
I want to be able to log in to school forum
So that I can create posts, comment and send messages

Scenario: I want to log in to Telerik Academy's school forum
Given element logInForumButton is present
When click logInForumButton element
And type username in signInNameField field
And type password in passwordInputField field
And click signInFormButton element
Then element schoolForumLogo is present