package stepdefinitions;

import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class StepDefinitions extends BaseStepDefinitions {
    UserActions actions = new UserActions();

    @Given("click $element element")
    @When("click $element element")
    @Then("click $element element")
    public void clickElement(String element) {
        actions.clickElement(element);
    }

    @Given("type $value in $name field")
    @When("type $value in $name field")
    @Then("type $value in $name field")
    public void typeInField(String value, String field) {
        actions.waitForElementVisible(field,20);
        actions.typeValueInField(value, field);
    }

    @Given("element $locator is present")
    @When("element $locator is present")
    @Then("element $locator is present")
    public void assertElementPresent(String locator) {
        actions.waitForElementVisible(locator, 10);
        actions.assertElementPresent((locator));
    }
//    @Given("element $locator is not present")
//    @When("element $locator is not present")
//    @Then("element $locator is not present")
//    public void assertElementNotPresent(String locator) {
//        actions.assertElementNotPresent((locator));
//    }
}