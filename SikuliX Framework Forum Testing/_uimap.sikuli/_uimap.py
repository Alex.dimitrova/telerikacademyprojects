########################################################
# UI map for XYZ
########################################################
from sikuli import *
########################################################


class Comment:
    find_topic_in_forum = "topic_created"
    topic = "topic.png"
    reply = "reply.png"
    type_subject = "type_subject.png"
    edit = "edit_button.png"
    more_options = "show_more.png"
    flag = "flag.png"
    flag_reason = "flag_reason.png"
    bookmark = "bookmark.png"
    cancel = "cancel_button.png"
    current_comment = "current_comment.png"
    save_edit = "save_edit.png"
    flag_check = "flag_check.png"
    flag_topic = "flag_topic.png"
    flagged_spam = "flagged_spam.png"
    comment = "comment_posted.png"
    flag_post = "flag_post.png"

class Search:
    search_button = "search_button.png"
    search_field = "search_field.png"