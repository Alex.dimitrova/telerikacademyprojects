from _lib import *


class SmokeTests(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass
        
    def test_TC59_create_comment(self):
        ForumStart.StartBrowser()
        Navigate.FindTopicInPage()
        assert(exists(Comment.topic))
        Navigate.FindCommentInTopic()
        wait(Comment.reply, 2)
        click(Comment.reply)
        type("Testing creation of a comment")
        click(Comment.reply)
        Navigate.GoBackInPage()


    def test_TC60_edit_comment(self):
        Navigate.FindTopicInPage()
        Navigate.FindCommentInTopic()
        click(Comment.edit)
        assert(exists(Comment.current_comment))
        click(Comment.current_comment)
        Navigate.DeleteText()
        type("Editing comment")
        click(Comment.save_edit)
        wait(5)
    
    def test_TC61_flag_comment(self):
        click(Comment.more_options)
        wait(2)
        click(Comment.flag_topic)
        assert(exists(Comment.flag_reason))
        click(Comment.flag_check)
        wait(Comment.flag_post,2)
        click(Comment.flag_post)
        assert(exists(Comment.flagged_spam))
        ForumStart.CloseBrowser()
    

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(SmokeTests)
    
   
    outfile = open("Report.html", "w")
    runner = HTMLTestRunner.HTMLTestRunner(stream=outfile, title='SmokeTests Report' )
    runner.run(suite)
    outfile.close()