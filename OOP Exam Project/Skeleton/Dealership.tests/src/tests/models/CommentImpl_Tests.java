package tests.models;

import com.telerikacademy.dealership.models.CommentImpl;
import com.telerikacademy.dealership.models.contracts.Comment;
import org.junit.Assert;
import org.junit.Test;

public class CommentImpl_Tests {

    @Test
    public void CommentImpl_ShouldImplementCommentInterface() {
        CommentImpl comment = new CommentImpl("content", "author");
        Assert.assertTrue(comment instanceof Comment);
    }

    @Test(expected = NullPointerException.class)
    public void Constructor_ShouldThrow_WhenContentIsNull() {
        CommentImpl comment = new CommentImpl(null, "author");
    }

    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenContentIsBelow3() {
        CommentImpl comment = new CommentImpl("co", "author");
    }

    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenContentIsAbove200() {
        CommentImpl comment = new CommentImpl(
                "baYu0Hwt22wQADzheGXCJcMzCZr1AySeVTW979CuoJBeGnw5WIBBRgQjMDUvk2cdgsAjQ5zbU0O" +
                        "c9PIWMlYp1fij6SNc4OzQ2bqyL2ST8zCGEKSQAFSyzvEW4nzxtnwcLJDAjMyM1Xot0jWDYFoq3Ymcj2" +
                        "lWIfiQXul9uybEiHcSpGnXKPBg09FWZqj7pR8KL48FN3prq", "author");
    }
}
