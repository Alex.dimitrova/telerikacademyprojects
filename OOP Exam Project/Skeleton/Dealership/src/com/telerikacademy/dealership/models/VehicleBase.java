package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {

    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";

    //constants
    private static final String NULL_TYPE_EXCEPTION = "CANNOT BE NULL";
    private static final int MAKE_MIN_LENGTH = 2;
    private static final int MAKE_MAX_LENGTH = 15;
    private static final String MAKE_EXCEPTION = "Make must be between 2 and 15 characters long!";
    private static final int MODEL_MIN_LENGTH = 1;
    private static final int MODEL_MAX_LENGTH = 15;
    private static final String MODEL_LENGTH_EXCEPTION = "Model must be between 1 and 15 characters!";
    private static final double MIN_PRICE = 0.0;
    private static final double MAX_PRICE = 100000.00;
    private static final String PRICE_EXCEPTION = "Price must be between 0.0 and 1000000.0!";

    // fields
    private List<Comment> comments;
    private String make;
    private String model;
    private double price;
    private VehicleType vehicleType;

    // constructor
    public VehicleBase(String make, String model, double price, VehicleType vehicleType) {
        setMake(make);
        setModel(model);
        setPrice(price);
        setVehicleType(vehicleType);
        comments = new ArrayList<>();
        int wheels = vehicleType.getWheelsFromType();
    }

    protected static String getNullTypeException() {
        return NULL_TYPE_EXCEPTION;
    }

    protected static int getMakeMinLength() {
        return MAKE_MIN_LENGTH;
    }

    protected static int getMakeMaxLength() {
        return MAKE_MAX_LENGTH;
    }

    protected static String getMakeException() {
        return MAKE_EXCEPTION;
    }

    protected static double getMinPrice() {
        return MIN_PRICE;
    }

    protected static double getMaxPrice() {
        return MAX_PRICE;
    }

    protected static String getPriceException() {
        return PRICE_EXCEPTION;
    }

    protected static int getModelMinLength() {
        return MODEL_MIN_LENGTH;
    }

    protected static int getModelMaxLength() {
        return MODEL_MAX_LENGTH;
    }

    protected static String getModelLengthException() {
        return MODEL_LENGTH_EXCEPTION;
    }

    private void setMake(String make) {
        if (make == null)
            throw new IllegalArgumentException(getNullTypeException());
        if (make.length() < getMakeMinLength() || make.length() > getMakeMaxLength()) {
            throw new IllegalArgumentException(getMakeException());
        } else {
            this.make = make;
        }
    }
    private void setModel(String model) {
        if (model == null) {
            throw new IllegalArgumentException(getNullTypeException());
        }
            if (model.length() < getModelMinLength() || model.length() > getModelMaxLength()){
                throw new IllegalArgumentException(getModelLengthException());
        }
        this.model = model;
    }

    private void setPrice(double price) {
        if (price < getMinPrice() || price > getMaxPrice()) {
            throw new IllegalArgumentException(getPriceException());
        } else {
            this.price = price;
        }
    }
    public VehicleType getVehicleType() {
        return vehicleType;
    }

    private void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        builder.append(String.format("  %s: %s", MAKE_FIELD, getMake())).append(System.lineSeparator());
        builder.append(String.format("  %s: %s", MODEL_FIELD, getModel())).append(System.lineSeparator());
        builder.append(String.format("  %s: %s", WHEELS_FIELD, getWheels())).append(System.lineSeparator());

        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());

        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }

    //package private - provides way to implement the base functionalities and it is inheritable:
    protected abstract String printAdditionalInfo();

    private String printComments()
    {
        StringBuilder builder = new StringBuilder();

        if (comments.size() <= 0)
        {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        }
        else
        {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());

            for (Comment comment : comments)
            {
                builder.append(comment.toString());
            }

            builder.append(String.format("%s", COMMENTS_HEADER));
        }

        return builder.toString();
    }

    @Override
    public int getWheels() {
        return vehicleType.getWheelsFromType();
    }

    @Override
    public VehicleType getType() {
        return vehicleType;
    }

    @Override
    public String getMake() {
        return make;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public void removeComment(Comment comment) {
        this.comments.remove(comment);

    }

    @Override
    public void addComment(Comment comment) {
        this.comments.add(comment);

    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public double getPrice() {
        return price;
    }
}
