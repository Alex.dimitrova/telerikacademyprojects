package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Truck;

public class TruckImpl extends VehicleBase implements Truck{

    //constants
    private static final int MIN_WEIGHT_CAPACITY = 1;
    private static final int MAX_WEIGHT_CAPACITY = 100;
    private static final String WEIGHT_CAPACITY_EXCEPTION = "Weight capacity must be between 1 and 100!";

    //field
    private int weightCapacity;


    private void setWeightCapacity(int weightCapacity) {
        if (weightCapacity < getMinWeightCapacity() || weightCapacity > getMaxWeightCapacity()){
            throw new IllegalArgumentException(getWeightCapacityException());
        }
        this.weightCapacity = weightCapacity;
    }

    public TruckImpl(String make, String model, double price, int weightCapacity){
        super(make, model, price, VehicleType.TRUCK);
        setWeightCapacity(weightCapacity);

    }
    protected static int getMinWeightCapacity() {
        return MIN_WEIGHT_CAPACITY;
    }

    protected static int getMaxWeightCapacity() {
        return MAX_WEIGHT_CAPACITY;
    }

    protected static String getWeightCapacityException() {
        return WEIGHT_CAPACITY_EXCEPTION;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Weight Capacity %st", getWeightCapacity());
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }

    //look in DealershipFactoryImpl - use it to create proper constructor

}
