package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;

public class CarImpl extends VehicleBase implements Car{

    //constants
    private static final int MIN_SEATS = 1;
    private static final int MAX_SEATS = 10;
    private static final String SEATS_EXCEPTION = "Seats must be between 1 and 10!";

    //field
    private int seats;


    private void setSeats(int seats) {
        if (seats < getMinSeats() || seats > getMaxSeats()) {
            throw new IllegalArgumentException(getSeatsException());
        } else {
            this.seats = seats;
        }
    }

    protected static int getMinSeats() {
        return MIN_SEATS;
    }

    protected static int getMaxSeats() {
        return MAX_SEATS;
    }

    protected static String getSeatsException() {
        return SEATS_EXCEPTION;
    }

    //constructor
    public CarImpl(String make, String model, double price, int seats){
        super(make, model, price, VehicleType.CAR);
        setSeats(seats);
    }
    @Override
    protected String printAdditionalInfo() {
        return String.format("  Seats: %d"+System.lineSeparator(), getSeats());
    }


    @Override
    public int getSeats() {
        return seats;
    }
    //look in DealershipFactoryImpl - use it to create proper constructor


}
