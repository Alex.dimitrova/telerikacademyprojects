package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

public class MotorcycleImpl extends VehicleBase implements Motorcycle{

    //constants
    private static final int CATEGORY_MIN_LENGTH = 3;
    private static final int CATEGORY_MAX_LENGTH = 10;
    private static final String CATEGORY_EXCEPTION = "Category must be between 3 and 10 characters long!";

    //field
    private String category;

    private void setCategory(String category) {
        if (category==null){
            throw new IllegalArgumentException(getNullTypeException());
        }
        if (category.length() < getCategoryMinLength() || category.length() > getCategoryMaxLength()) {
            throw new IllegalArgumentException(getCategoryException());
        } else {
            this.category = category;
        }
    }

    public MotorcycleImpl(String make, String model, double price, String category){
        super(make, model, price, VehicleType.MOTORCYCLE);
        setCategory(category);

    }


    protected static int getCategoryMinLength() {
        return CATEGORY_MIN_LENGTH;
    }

    protected static int getCategoryMaxLength() {
        return CATEGORY_MAX_LENGTH;
    }

    protected static String getCategoryException() {
        return CATEGORY_EXCEPTION;
    }
    @Override
   protected String printAdditionalInfo() {
        return String.format("  Category: %s" +System.lineSeparator(), getCategory());
    }


    @Override
    public String getCategory() {
        return category;
    }

}
