package com.telerikacademy.dealership.core.contracts;

import com.telerikacademy.dealership.commands.contracts.Command;

public interface CommandFactory {
    Command createCommand(String commandTypeAsString, DealershipFactory dealershipFactory, DealershipRepository dealershipRepository);
}
