const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '3',
    '[1,2,3,4,5]',
    '[1,2,5,4,5]',
    '[1,3,3,4,5]'

];
const gets = this.gets || getGets(test);
const print = this.print || console.log;
let n = +gets();

for(let i = 0; i<n; i++){
    let numbers = gets().split(",").map(Number);
    let IsSorted = true;

    for(let j = 0; j < numbers.length; j++){
        if(numbers[j-1] > numbers[j]){
            IsSorted = false;
        }
    }
    print(IsSorted);
}