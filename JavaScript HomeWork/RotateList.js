const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '1,2,0,3,0,3,5',
];
const gets = this.gets || getGets(test);
const print = this.print || console.log;

let numbers = gets().split(",").map(Number);
let index = +gets();

while(index--){
    numbers.push(numbers.shift());
}
print(numbers)