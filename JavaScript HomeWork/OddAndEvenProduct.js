const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '5',
    '4',
    '3',
    '2',
    '5',
    '2',
];
const gets = this.gets || getGets(test);
const print = this.print || console.log;

let numbers = +gets();

let evenProduct = 1;
let oddProduct = 1;


for (i = 1; i <= numbers; i++) {
    let currentNum = +gets();

    if (i % 2 === 0) {
        evenProduct *= currentNum;
    } else {
        oddProduct *= currentNum;
    }
}
if (evenProduct === oddProduct) {
    print(`yes ${evenProduct}`);
} else {
    print(`no ${oddProduct} ${evenProduct}`)
}
