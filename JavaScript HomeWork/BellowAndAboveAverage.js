const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '3,-12,0,0,13,5,1,0,-2',
];
const gets = this.gets || getGets(test);
const print = this.print || console.log;

let numbers = gets().split(',').map(Number);
let below = [];
let above = [];
let findAverage = numbers => numbers.reduce((a, b) => a + b, 0)/ numbers.length;
let average = findAverage(numbers);

for(let i=0; i<numbers.length; i++){
    let currentNum = numbers[i];

    if(currentNum>average){
       above.push(currentNum);
    }else if(currentNum ===0 && average ===0){
        continue;
    }
    else{
        below.push(currentNum);
    }
}
print(`avg: ${average.toFixed(2)}`);
print(`below: ${below}`);
print(`above: ${above}`);
