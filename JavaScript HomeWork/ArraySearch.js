const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '1,2,3,3,5',
];
const gets = this.gets || getGets(test);
const print = this.print || console.log;

let numbers = gets().split(",").map(Number);
let current = [];
let missing = [];

for(let i=0; i < numbers.length; i++){
    current[i] = i;
}
let existing = [...new Set(numbers)];

numbers=[];
numbers = existing;

for(let j=1; j<=current.length; j++){
    if(!numbers.includes(j)){
        missing.push(j)
    }
}
print(missing)