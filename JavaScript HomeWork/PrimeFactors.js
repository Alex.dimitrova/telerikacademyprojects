const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '12',
];
const gets = this.gets || getGets(test);
const print = this.print || console.log;

let number = +gets();
let PrimeFactors = [];

for(i=2; i<=number; i++){
   while (number % i === 0){
   PrimeFactors.push(i);
   number /= i;
   }
}
PrimeFactors.forEach(function(PrimeFactors){
    print(PrimeFactors);
});