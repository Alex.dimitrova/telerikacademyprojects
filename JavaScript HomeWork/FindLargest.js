const getGets = (arr) => {
  let index = 0;

  return () => {
    const toReturn = arr[index];
    index += 1;
    return toReturn;
  };
};
// this is the test
const test = [
  '6',
  '9',
  '11',
  '3',
  '2',
  '1',
  '8'
];

const gets = this.gets || getGets(test);
const print = this.print || console.log;

let number = +gets()
let arr = new Array(number);

for (i = 0; i<number; i++) {
  arr.push(+gets());
}
let result = arr.sort(function(a, b) {
  return b - a;
});
let first = result.slice(0,1);
let second = result.slice(1,2);
let thrird = result.slice(2,3);

print(`${first}, ${second} and ${thrird}`)