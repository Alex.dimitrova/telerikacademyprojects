const getGets = (arr) => {
    let index = 0;
  
    return () => {
      const toReturn = arr[index];
      index += 1;
      return toReturn;
    };
  };
  // this is the test
  const test = [
    '4',
    '5',
    '7',
    '3',
    '6',
    '3',
  ];
  const gets = this.gets || getGets(test);
const print = this.print || console.log;


let numbers = +gets();
let arr = [];

for (i = 0; i<numbers; i++) {
    arr.push(+gets());
}
let max = arr.reduce(function(a, b) {
    return Math.max(a, b);
});
print(max)