const getGets = (arr) => {
    let index = 0;

    return () => {
        const toReturn = arr[index];
        index += 1;
        return toReturn;
    };
};
// this is the test
const test = [
    '1,2,0,3,0,3,5',
];
const gets = this.gets || getGets(test);
const print = this.print || console.log;

let numbers = gets().split(",").map(Number);
let zeros = [];
let index = 0;
while (numbers.includes(0)){
    if(numbers[index] ===0){
        numbers.splice(index, 1);
        zeros.push(0);
        index = 0;
    }else{
        index++;
        continue;
    }
}

let sortedNums = numbers.concat(zeros);
print(sortedNums);