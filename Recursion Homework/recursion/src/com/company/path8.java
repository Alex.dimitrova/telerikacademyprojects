package com.company;

import java.util.Scanner;

public class path8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = Integer.parseInt(scanner.nextLine());
        System.out.println(countEight(num));
    }

    private static int countEight(int num) {
        if (num <= 0) {
            return 0;
        }
        if (num % 10 == 8) {
            if (num / 10 % 10 == 8)
                return 2 + countEight(num / 10);
            return 1 + countEight(num / 10);
        }
            return countEight(num / 10);
        }
    }
