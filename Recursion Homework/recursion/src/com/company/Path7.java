package com.company;

import java.util.Scanner;

public class Path7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = Integer.parseInt(scanner.nextLine());
        System.out.println(countSeven(num));
    }

    private static int countSeven(int num) {
        if (num == 0) {
            return 0;
        }
        if (num % 10 == 7) {
            return 1 + countSeven(num /10);
        }
        return countSeven(num/10);
    }
}
