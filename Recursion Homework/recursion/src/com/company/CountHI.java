package com.company;

import java.util.Scanner;

public class CountHI {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        System.out.println(countHi(text));
    }

    private static int countHi(String text) {
        if (text.length() < 2){
            return 0;
        }
        if (text.endsWith("hi")){
            return 1 + countHi(text.substring(0, text.length()-2));
        }
        else {
            return countHi(text.substring(0, text.length()-1));
        }


    }
}
