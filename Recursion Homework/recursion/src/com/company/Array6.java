package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Array6 {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        String[] input = scanner.next().split(",");

        ArrayList<Integer> numbers = new ArrayList<>();
        for (String number: input) {
            numbers.add(Integer.parseInt(number));

        }
        int index = scanner.nextInt();
        System.out.println(array6(numbers,index));



    }
    private static boolean array6(ArrayList<Integer> numbers, int index) {

        if (index==numbers.size()){
            return false;
        }
        if (numbers.get(index)==6)
            return true;
        return array6(numbers, index+1);
    }

}
