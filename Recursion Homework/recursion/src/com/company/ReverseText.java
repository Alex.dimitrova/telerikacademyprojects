package com.company;

import java.util.Scanner;

public class ReverseText {
    public static void main(String[] args) {
        //Scanner sc = new Scanner(System.in);
        System.out.println(reverseText("this"));
    }

    private static String reverseText(String text) {
        if (text.length() == 0) {
            return text;
        }
        return reverseText(text.substring(1)) + text.substring(0, 1);
    }
}

