package com.company;

import java.util.Scanner;

public class bunny {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int bunnies = scanner.nextInt();
        System.out.println(Bunny(bunnies));

    }

    private static int Bunny(int number) {
        if (number <= 0) {
            return 0;
        }
        if (number % 2 == 0) {
            return Bunny(number - 1) + 3;
        }
        else{
            return  Bunny(number - 1) + 2;
        }
    }
}
