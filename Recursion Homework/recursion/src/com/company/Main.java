package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        /*recursivePrint(3);
        System.out.println(recursiveSum(10));

    }

    private static int recursiveSum(int number) {
        if (number==0){
            return 0;
        }
        return recursiveSum(number-1) +number;
    }

    private static void recursivePrint(int number) {
        if (number == 0) {

            return;
        }
        System.out.println(number);
        recursivePrint(number-1);
        System.out.println(number);
    }*/
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        Factorial(number);
        System.out.println(Factorial(number));

    }

    private static int Factorial(int number) {

        if (number <= 1) {
            return 1;
        }
        return number * Factorial(number - 1);

    }

}