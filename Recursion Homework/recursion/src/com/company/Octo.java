package com.company;

public class Octo {
    public static void main(String[] args) {
        System.out.println(octo(100));
    }

    private static int octo(int number) {
        if (number <= 0) {
            return 0;
        }
        int result = 0;
        result = octo(number - 1) + 8;
        return result;
    }
}
