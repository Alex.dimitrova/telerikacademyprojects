package com.company;

import java.util.Scanner;

public class ChangePi {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String character = scanner.nextLine();
        System.out.println(changePi(character));

    }
    private static String changePi(String text){
        if (text.length() < 2)
            return text;
        if (text.substring(0, 2).equals("pi"))
            return "3.14" + changePi(text.substring(2));
        return text.charAt(0) + changePi(text.substring(1));
    }
}
